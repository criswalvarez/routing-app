import { Injectable } from '@angular/core';
import { User } from '../models/user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user : User 

  constructor() { }

  login(email: string, password : string){
    if(email == this.user.email && password == this.user.password){
      return true;
    }else{
      return false;
    }
  }

  register(email: string, password : string){
    this.user = new User()
    this.user.email = email
    this.user.password = password
  }
}
