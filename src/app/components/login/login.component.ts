import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth : AuthService) { }

  ngOnInit() {
  }

  login(email : string, password : string){
    if(this.auth.login(email, password)){
      alert("login correcto")
    }else{
      alert("login incorrecto")
    }

  }
}
