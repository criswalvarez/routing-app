import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  ngOnInit(): void {
   
  }

  constructor(private router : Router ){}

  goToLogin(){
    this.router.navigate(['/login'])
  }

  goToRegister(){
    this.router.navigate(['/register'])
  }

}
