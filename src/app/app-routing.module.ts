import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { MainComponent } from './components/main/main.component';

//definimos las rutas principales
//ej : /user /login /loquesea
//ng g m movies --routing //general modulos con rutas
const routes: Routes = [
  { path : 'login', component: LoginComponent},
  { path : 'register', component: RegisterComponent },
  { path : '**' , component : MainComponent},
  { path : 'main' , component : MainComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
